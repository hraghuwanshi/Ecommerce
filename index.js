import productSet from "./Utils/product.js";
import productObj from "./Utils/productObj.js";
import cartSet from "./Utils/cart.js";
const product = document.querySelector(".products");
const cart= document.querySelector(".carts");
const totalCost=document.querySelector(".totalCost");
productObj.forEach((obj)=>{
    product.append(productSet(obj));
});

// const cartCost= document.querySelector(".cartCost");
// //adding + - functionality
// const minus= document.querySelectorAll(".minus");
// minus[0].addEventListener("click",(e)=>{
//     const value=e.target.nextSibling;
//     if(Number(value.innerText)>1)
//     value.innerText= Number(value.innerText)-1;

   
// })

// const plus= document.querySelectorAll(".plus");
// plus[0].addEventListener("click",(e)=>{
//     const value=e.target.previousSibling;
//     value.innerText= Number(value.innerText)+1;

//     // console.log(Number(cartCost.innerText.slice(1)));
//     // cartCost.innerText= `$${Number(value.innerText)*Number(cartCost.innerText.slice(1))}`;
// })


const carts= document.querySelector(".carts");
carts.addEventListener("click",(e)=>{
    let noOfQuantityBefore=1;
    let noOfQuantityAfter=1;
    if(e.target.classList=="minus")
    {
        let value=e.target.nextSibling;
        console.log(value)
        noOfQuantityBefore= Number(value.innerText);
        if(Number(value.innerText)>1)
        noOfQuantityAfter= Number(value.innerText)-1;
        value.innerText=noOfQuantityAfter;
    }
    else  if(e.target.classList=="plus")
    {
        let value=e.target.previousSibling;
        console.log(value)
        noOfQuantityBefore= Number(value.innerText);
        noOfQuantityAfter= Number(value.innerText)+1;
        value.innerText=noOfQuantityAfter;
    }
    if(noOfQuantityAfter!=noOfQuantityBefore)
    {
        const cartPrice= e.target.parentNode.parentNode.nextSibling;
        const actualValue= Number(cartPrice.innerText.slice(1))/noOfQuantityBefore;
        const newValue= noOfQuantityAfter*actualValue;
        const oldValue= noOfQuantityBefore*actualValue;
        cartPrice.innerText=`$${newValue}`;
        totalCost.innerText= Number(totalCost.innerText)-oldValue+newValue;
    }

},true)
// console.log(carts);

//add to cart


product.addEventListener("click",(e)=>{
    console.log(e.target.parentNode);
    
    if(e.target.classList=="productButton")
    {
        const id=e.target.parentNode.id;
        if(productObj[id].added==0)
        {
            productObj[id].added=1;
            cart.append(cartSet(productObj[id]));
            totalCost.innerText= Number(totalCost.innerText)+productObj[id].cost;
            e.target.innerText="Added";
        }
    }
},true)