export default function cartSet({image,title,cost}){
    const carList= document.createElement("div");
    carList.setAttribute("class","cartList");

    const Image=document.createElement("img");
     Image.setAttribute("src",image)
     Image.setAttribute("class","cartImage");
     carList.append(Image);

     const headingQuantity=document.createElement("div");
     headingQuantity.setAttribute("class","headingQuantity");
     carList.append(headingQuantity);

     const heading=document.createElement("div");
     heading.setAttribute("class","heading");
     heading.innerText=title;
     headingQuantity.append(heading);

     const quantities=document.createElement("div");
     quantities.setAttribute("class","quantities");
     headingQuantity.append(quantities);

     const quantity=document.createElement("div");
     quantity.setAttribute("class","quantity");
     quantity.innerText="Quantity";
     quantities.append(quantity);

     const minus=document.createElement("button");
     minus.setAttribute("class","minus");
     minus.innerText="-";
     quantities.append(minus);

     const noOfQuantity=document.createElement("div");
     noOfQuantity.setAttribute("class","noOfQuantity");
     noOfQuantity.innerText=1;
     quantities.append(noOfQuantity);

     const plus=document.createElement("button");
     plus.setAttribute("class","plus");
     plus.innerText="+";
     quantities.append(plus);

     const cartCost=document.createElement("div");
     cartCost.setAttribute("class","cartCost");
    //  cartCost.innerHTML=
    //  `<div>$<div/>
    //  <div class="cartValue">${cost}<div/>
    //  <div class="cartPrice">${cost}<div/>` //how to hide this cost
     cartCost.innerText=`$${cost}`;
     carList.append(cartCost);
    
    
    
    // `<img src=${image} class="cartImage">
    // <div class="headingQuantity">
    //         <div class="heading">${title}</div> 
    //         <div class="quantities">
    //             <div class="quantity">Quantity</div>
    //             <div class="minus"> - </div>
    //             <div class="noOfQuantity">2</div>
    //             <div class="minus plus"> + </div>
    //         </div>
    // </div>
    // <div class="cartCost">${cost}</div>`
    return carList;
}