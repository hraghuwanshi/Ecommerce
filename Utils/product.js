function rating(noOfStar){
  const rating = document.createElement("div");
  rating.setAttribute("class", "rating");
  for(let i=0;i<5;i++)
  {
    const span = document.createElement("span");
    noOfStar>i?span.setAttribute("class", "fa fa-star fa-xl checked"):span.setAttribute("class", "fa fa-star fa-xl");
    rating.append(span);
  }
  
  return rating;
}
function productSet({id,image,title,description,noOfStar,cost}){
    const productList = document.createElement("div");
    productList.setAttribute("class", "productList");
    productList.setAttribute("id",id);


    const productItems = document.createElement("div");
    productItems.setAttribute("class", "productItems");
    productList.append(productItems);

    const productImage = document.createElement("img");
    productImage.setAttribute("class", "productImage");
    productImage.setAttribute("src",image);
    productItems.append(productImage);

    const productDetail = document.createElement("div");
    productDetail.setAttribute("class", "productDetail");
    productItems.append(productDetail);


    const heading = document.createElement("div");
    heading.setAttribute("class", "heading");
    heading.innerText=title;
    productDetail.append(heading);

    const Description = document.createElement("div");
    Description.setAttribute("class", "description");
    Description.innerText=description;
    productDetail.append(Description);

    

    const productCost= document.createElement("div");
    productCost.setAttribute("class", "productCost");
    productItems.append(productCost);

    productCost.append(rating(noOfStar));

    const Cost = document.createElement("div");
    Cost.setAttribute("class", "cost");
    Cost.innerText=`$ ${cost}`;
    productCost.append(Cost);

    const productButton = document.createElement("button");
    productButton.setAttribute("class", "productButton");
    productButton.innerText="Add To Cart";
    productList.append(productButton);
    
    // console.log(productList)
    return productList;
}

//why prepend not working 
// productList.innerHTML=`
// <div class="productItems">
//   <img class="productImage" src=${image} />
//   <div class="productDetail">
//     <div id="heading">${title}</div>
//     <div id="description"> ${description} </div>
//   </div>
//   <div class="productCost">
//     <div id="cost">$ ${cost}</div>
//   </div>
// </div>
// <div
//   id="productButton">
//   Add To Cart
// </div>`
// productList.getElementsByClassName(".productCost").prepend(rating(noOfStar));
export default productSet;
